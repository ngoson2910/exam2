import React from 'react';
import './App.css';
import 'h8k-components';

import Articles from './components/Articles';

const title = "Sorting Articles";

function App() {
    const [articles, setArticles] = React.useState([]);
    const [id, setId] = React.useState('');

    React.useEffect(() => {
        fetch(`https://jsonplaceholder.typicode.com/users/1/posts${id.trim() !== '' ? '?id='+ id.trim() : ''}`)
            .then(res => {
                return res.json()
            })
            .then(data => {
                setArticles(data);
            })
            .catch(error => console.log(error))
    }, [id])

    const handlSortDescById = () => {
        setArticles(prev => [...prev].sort((a,b) => {
            if(a.id > b.id) return -1;
            if(a.id < b.id) return 1;
            return 0;
        }));
    }
    const handlSortDescByTitle = () => {
        setArticles(prev => [...prev].sort((a,b) => {
            if(a.title > b.title) return -1;
            if(a.title < b.title) return 1;
            return 0;
        }));
    }

    return (
        <div className="App">
            <h8k-navbar header={title}></h8k-navbar>
            <div className="layout-row align-items-center justify-content-center my-20 navigation">
                <label className="form-hint mb-0 text-uppercase font-weight-light">Sort By</label>
                <button 
                    data-testid="most-upvoted-link" 
                    className="small"
                    onClick={handlSortDescById}
                >
                    Most ID
                </button>
                <button 
                    data-testid="most-recent-link" 
                    className="small"
                    onClick={handlSortDescByTitle}
                >
                    Most Title
                </button>
                <input 
                    value={id}
                    onChange={e => setId(e.target.value)}
                />
            </div>
            <Articles  articles={articles} />
        </div>
    );

}

export default App;
