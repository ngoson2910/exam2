import React from 'react';
import Article from './Article';

function Articles({articles}) {

    return (
        <div className="card w-50 mx-auto">
            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Body</th>
                </tr>
                </thead>
                <tbody>
                    {articles.map(article => (
                        <Article key={article.id} {...article} />
                    ))}
                </tbody>
            </table>
        </div>
    );

}

export default Articles;
