import React from 'react';

function Article({ id, title, body}) {
  return (
    <tr data-testid="article" key="article-index">
      <td data-testid="article-id">{id}</td>
      <td data-testid="article-title">{title}</td>
      <td data-testid="article-upvotes">{body}</td>
    </tr>
  )
}
export default Article;